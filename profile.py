import requests

from xhs_utils.xhs_util import get_headers, check_cookies, handle_profile_info, download_media, check_and_create_path, norm_str, save_user_detail

class Profile:
    def __init__(self, cookies=None, proxies=None):
        self.current_proxy_index = 0  # 用于跟踪当前代理的索引
        self.proxies = proxies  # 代理列表
        if cookies is None:
            self.cookies = check_cookies()
        else:
            self.cookies = cookies

    def get_next_proxy(self):
        # 获取下一个代理，并更新索引
        proxy = self.proxies[self.current_proxy_index]
        self.current_proxy_index = (self.current_proxy_index + 1) % len(self.proxies)
        return proxy

    # 个人信息主页
    def get_profile_info(self, url):
        headers = get_headers()
        proxy = self.get_next_proxy()  # 获取代理
        response = requests.get(url, headers=headers, cookies=self.cookies, proxies=proxy)
        html_text = response.text
        userId = url.split('/')[-1]
        profile = handle_profile_info(userId, html_text)
        return profile

    def save_profile_info(self, url):
        profile = self.get_profile_info(url)
        print(f'开始保存用户{profile.nickname}基本信息')
        userId = profile.userId
        nickname = norm_str(profile.nickname)
        path = f'./datas/{nickname}_{userId}'
        check_and_create_path(path)
        download_media(path, 'avatar', profile.avatar, 'image', '用户头像')
        save_user_detail(path, profile)
        print(f'User {nickname} 信息保存成功')
        return profile


    def main(self, user_url_list):
        for url in user_url_list:
            try:
                self.save_profile_info(url)
            except:
                print(f'user {url} 查询失败')


if __name__ == '__main__':
    profile = Profile()
    user_url_list = [
        'https://www.xiaohongshu.com/user/profile/59d44fd66eea883eff45747f',
        'https://www.xiaohongshu.com/user/profile/5a73c5fa4eacab4c4ccc9778',
        'https://www.xiaohongshu.com/user/profile/5b4c369611be104d815b1ba9',
        'https://www.xiaohongshu.com/user/profile/5ee237720000000001001a5d',
        'https://www.xiaohongshu.com/user/profile/58fd96255e87e71f726905f1',
        'https://www.xiaohongshu.com/user/profile/562ef75ae00dd861d410784a',
        'https://www.xiaohongshu.com/user/profile/5a747a7ce8ac2b215c98d8ef',
        'https://www.xiaohongshu.com/user/profile/5842afd75e87e7332ea90fda',
        'https://www.xiaohongshu.com/user/profile/6121f6e8000000000101753d',
        'https://www.xiaohongshu.com/user/profile/5406935db4c4d61f895aded2',
    ]
    profile.main(user_url_list)
