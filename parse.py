import csv
# CSV文件路径
csv_file_path = './TA人群KOL偏好0316.csv'

# 输出TXT文件路径
output_txt_file_path = 'id.txt'

# 打开CSV文件和输出文件
with open(csv_file_path, newline='', encoding='GBK') as csvfile, open(output_txt_file_path, 'w', newline='') as txtfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        # 假设列是以0为起始索引的
        second_column_value = row[5]
        # 写入第二列的值到TXT文件，每个值后面跟一个换行符
        txtfile.write(second_column_value + '\n')