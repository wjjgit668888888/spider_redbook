import pymysql


class DatabaseManager:
    def __init__(self, db_config):
        # 初始化数据库连接
        self.connection = pymysql.connect(**db_config)
        self.cursor = self.connection.cursor()
        print("Creating tables if not exists...")
        self.ensure_table_exists()

    def ensure_table_exists(self):
        # 创建notes表，如果不存在
        create_table_query = """
        CREATE TABLE IF NOT EXISTS notes (
            note_id VARCHAR(255) PRIMARY KEY,
            title VARCHAR(255),
            content TEXT,
            user_id VARCHAR(255),
            nickname VARCHAR(255)
        );
        """
        self.cursor.execute(create_table_query)
        self.connection.commit()

    def insert_note_data(self, note_id, title, content, user_id, nickname):
        # 插入笔记数据到notes表
        insert_query = """
        INSERT INTO notes (note_id, title, content, user_id, nickname)
        VALUES (%s, %s, %s, %s, %s)
        ON DUPLICATE KEY UPDATE
        title = VALUES(title),
        content = VALUES(content),
        user_id = VALUES(user_id),
        nickname = VALUES(nickname);  # 更新nickname字段
        """
        self.cursor.execute(insert_query, (note_id, title, content, user_id, nickname))
        self.connection.commit()

    def close(self):
        # 关闭数据库连接
        self.cursor.close()
        self.connection.close()
