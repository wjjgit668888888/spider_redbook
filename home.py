# 导入所需的库：requests用于发起HTTP请求，还有一些自定义模块用于处理用户资料和实用工具函数。
import json
from static.config import PROXIES_LIST

import requests
from profile import Profile
from xhs_utils.xhs_util import get_headers, get_params, js, check_cookies
import sys
import time  # 导入time模块
import pandas as pd

import os
data_folder = './data'  # 根据需要调整路径
os.makedirs(data_folder, exist_ok=True)


# 定义一个名为'Home'的类，用来封装与用户资料及其帖子（笔记）相关的功能。
class Home:
    def __init__(self, cookies=None, proxies=None):
        self.current_proxy_index = 0  # 用于跟踪当前代理的索引
        self.proxies = proxies  # 代理列表
        # 构造函数，用于初始化类实例。如果没有提供cookies参数，则调用check_cookies函数获取cookies。
        if cookies is None:
            self.cookies = check_cookies()
        else:
            self.cookies = cookies
        # 定义获取用户帖子的API URL。
        self.more_url = 'https://edith.xiaohongshu.com/api/sns/web/v1/user_posted'
        # 创建Profile实例，用于获取用户资料。
        self.profile = Profile(cookies=check_cookies(), proxies=PROXIES_LIST)
        # 获取请求头部信息。
        self.headers = get_headers()
        # 获取请求参数。
        self.params = get_params()

    def get_next_proxy(self):
        # 获取下一个代理，并更新索引
        proxy = self.proxies[self.current_proxy_index]
        self.current_proxy_index = (self.current_proxy_index + 1) % len(self.proxies)
        return proxy

    def get_all_note_info(self, url):
        # 从给定的URL获取用户的所有笔记信息。
        time.sleep(1)  # 等待1秒
        profile = self.profile.get_profile_info(url)  # 获取用户资料信息。
        note_id_list = []  # 初始化笔记ID列表。
        user_id = profile.userId  # 从用户资料中获取用户ID。
        cursor = ''  # 初始化游标，用于分页。
        # 更新请求参数。
        self.params['user_id'] = user_id
        self.params['cursor'] = cursor
        while True:
            # 构造API请求URL。
            api = f"/api/sns/web/v1/user_posted?num=30&cursor={cursor}&user_id={user_id}&image_scenes="
            # 使用自定义js工具调用API获取响应头中的特定信息。
            ret = js.call('get_xs', api, '', self.cookies['a1'])
            self.headers['x-s'], self.headers['x-t'] = ret['X-s'], str(ret['X-t'])
            # 确保在发起请求时使用代理
            proxy = self.get_next_proxy()
            # 使用代理发起请求
            response = requests.get(self.more_url, headers=self.headers, cookies=self.cookies, params=self.params,
                                    proxies=proxy)
            if response.status_code != 200:
                print(f"Failed to fetch notes for user {user_id}, HTTP status code: {response.status_code}")
                print("Response body:", response.text)  # 打印响应内容
                break
            res = response.json()  # 解析响应内容为JSON格式。
            if 'data' not in res or 'notes' not in res['data'] or not res['data']['notes']:
                print(f"No notes found for user {user_id} or 'notes' key is missing in the response.")
                break
            data = res["data"]
            # 更新游标和其他信息以获取下一页数据。
            cursor, has_more, note_list = data["cursor"], data["has_more"], data["notes"]
            self.params['cursor'] = cursor
            for note in note_list:
                note_id_list.append(note['note_id'])  # 将笔记ID添加到列表中。
            if not has_more:
                break
        return note_id_list, profile  # 返回笔记ID列表和用户资料。

    def save_all_note_info(self, url):
        # 保存给定URL的用户的所有笔记信息。
        note_id_list, profile = self.get_all_note_info(url)  # 获取所有笔记ID和用户资料。

        total_notes = len(note_id_list)  # 获取总笔记数
        if total_notes == 0:
            print(f"用户{profile.nickname}没有笔记")
            return profile
        print(f"正在处理用户{profile.nickname}的笔记...")

        # 准备一个列表来存储所有笔记的标题和内容
        notes_data = []

        for index, note_id in enumerate(note_id_list, start=1):
            # 遍历笔记ID列表，获取每个笔记的内容。
            note_content = self.get_note_content(note_id)
            if note_content:
                # 假设note_content是标题和内容的组合
                title, content = note_content.split('\n', 1) if '\n' in note_content else ('', note_content)
                # 添加到notes_data列表
                notes_data.append({'Title': title, 'Content': content})
            progress = (index / total_notes) * 100  # 计算处理进度百分比
            sys.stdout.write(f'\r[{index}/{total_notes} 完成{progress:.2f}%]')
            sys.stdout.flush()
        print()  # 完成一个用户的笔记处理后换行

        # 使用pandas将notes_data列表保存到Excel文件中
        if notes_data:
            df = pd.DataFrame(notes_data)
            # 使用用户昵称作为文件名（确保文件名合法，可能需要进一步处理）
            filename = os.path.join(data_folder, f"{profile.nickname}.xlsx".replace('/', '_').replace('\\', '_'))
            df.to_excel(filename, index=False)
        return profile

    def get_note_content(self, note_id):
        note_url = 'https://edith.xiaohongshu.com/api/sns/web/v1/feed'
        data = {"source_note_id": note_id, "image_scenes": ["CRD_PRV_WEBP", "CRD_WM_WEBP"]}
        data = json.dumps(data, separators=(',', ':'))

        # 更新headers['x-s']和headers['x-t']的值
        ret = js.call('get_xs', '/api/sns/web/v1/feed', data, self.cookies['a1'])
        self.headers['x-s'], self.headers['x-t'] = ret['X-s'], str(ret['X-t'])
        # 确保在发起请求时使用代理
        proxy = self.get_next_proxy()
        # 使用代理发起POST请求
        response = requests.post(note_url, headers=self.headers, cookies=self.cookies, data=data, proxies=proxy)
        if response.status_code == 200:
            note_data = response.json()
            try:
                # 假设我们需要的笔记内容在响应的第一个项目中
                note_info = note_data['data']['items'][0]
                # 获取笔记标题，如果不存在则默认为空字符串
                note_title = note_info['note_card'].get('title', '').strip()
                # 获取笔记内容
                note_desc = note_info['note_card']['desc'].strip().replace('\n', ' ')
                # 如果存在标题，则将标题作为笔记内容的第一行
                if note_title:
                    return f"{note_title}\n{note_desc}"
                else:
                    return note_desc
            except (KeyError, IndexError) as e:
                print(f"Error fetching note content for note ID {note_id}: {e}")
        return None


    def main(self, url_list):
        total = len(url_list)
        for index, url in enumerate(url_list, start=1):
            try:
                print(f"{index}/{total}: {url}")
                self.save_all_note_info(url)
            except Exception as e:
                print(f'Failed to process user from {url}: {e}')


if __name__ == '__main__':

    home = Home(cookies=check_cookies(), proxies=PROXIES_LIST)
    url_list = [
        'https://www.xiaohongshu.com/user/profile/5a13b53b4eacab190f3d0667',
        'https://www.xiaohongshu.com/user/profile/5a73c5fa4eacab4c4ccc9778',
        'https://www.xiaohongshu.com/user/profile/5b4c369611be104d815b1ba9',
        'https://www.xiaohongshu.com/user/profile/5ee237720000000001001a5d',
        'https://www.xiaohongshu.com/user/profile/58fd96255e87e71f726905f1',
        'https://www.xiaohongshu.com/user/profile/562ef75ae00dd861d410784a',
        'https://www.xiaohongshu.com/user/profile/5a747a7ce8ac2b215c98d8ef',
        'https://www.xiaohongshu.com/user/profile/5842afd75e87e7332ea90fda',
        'https://www.xiaohongshu.com/user/profile/6121f6e8000000000101753d',
        'https://www.xiaohongshu.com/user/profile/5406935db4c4d61f895aded2',
        'https://www.xiaohongshu.com/user/profile/572c0cbd5e87e70c4bf3fd56',
        'https://www.xiaohongshu.com/user/profile/5bbd8de666b4690001dcf9a0',
        'https://www.xiaohongshu.com/user/profile/52d8c541b4c4d60e6c867480',
        'https://www.xiaohongshu.com/user/profile/5abb15dd4eacab6fbea4d384',
        'https://www.xiaohongshu.com/user/profile/58d50fa76a6a693274126ec3',
        'https://www.xiaohongshu.com/user/profile/5a613df44eacab5aefbca8fc',
        'https://www.xiaohongshu.com/user/profile/5f9847230000000001007825',
        'https://www.xiaohongshu.com/user/profile/5956b4f750c4b462fbcb94fe',
        'https://www.xiaohongshu.com/user/profile/5a4ee75ce8ac2b4cb41b1649',
        'https://www.xiaohongshu.com/user/profile/5a94c39ce8ac2b1a31e83e10',
        'https://www.xiaohongshu.com/user/profile/56659beb82718c1154e6efa4',
        'https://www.xiaohongshu.com/user/profile/5987f2825e87e71fcabd346f',
        'https://www.xiaohongshu.com/user/profile/59761be382ec395be771e7c5',
        'https://www.xiaohongshu.com/user/profile/5fb23b21000000000100bee3',
        'https://www.xiaohongshu.com/user/profile/566d23f25e87e7541b29d3c9',
        'https://www.xiaohongshu.com/user/profile/56cfa9b74775a74e6f8bdb1e',
        'https://www.xiaohongshu.com/user/profile/6357c096000000001802aa80',
        'https://www.xiaohongshu.com/user/profile/5c69583a00000000120305d2',
        'https://www.xiaohongshu.com/user/profile/582d96f06a6a694cc47308c3',
        'https://www.xiaohongshu.com/user/profile/593032945e87e77791e03696',
        'https://www.xiaohongshu.com/user/profile/5a577d264eacab62ff4eeb9e',
        'https://www.xiaohongshu.com/user/profile/5dc0e8c90000000001004610',
        'https://www.xiaohongshu.com/user/profile/5fe0e8470000000001005596',
        'https://www.xiaohongshu.com/user/profile/5c820a62000000001203893b',
        'https://www.xiaohongshu.com/user/profile/58e58b3982ec397bae251518',
        'https://www.xiaohongshu.com/user/profile/562f8c8ef53ee026a3c94b5e',
        'https://www.xiaohongshu.com/user/profile/5e2ef604000000000100476b',
        'https://www.xiaohongshu.com/user/profile/5c99a82c0000000012012680',
        'https://www.xiaohongshu.com/user/profile/5d26c62100000000120236c3',
        'https://www.xiaohongshu.com/user/profile/5594a6665894464be38ce6ed',
        'https://www.xiaohongshu.com/user/profile/5a7d3ed311be106d0306e7d6',
        'https://www.xiaohongshu.com/user/profile/5daec1a7000000000100420a',
        'https://www.xiaohongshu.com/user/profile/5db15aed000000000100442b',
        'https://www.xiaohongshu.com/user/profile/56826634cb35fb7e671d6bfc',
        'https://www.xiaohongshu.com/user/profile/58aa7fd882ec396aad04d4c9',
        'https://www.xiaohongshu.com/user/profile/573d642d6a6a6929995b37e1',
        'https://www.xiaohongshu.com/user/profile/5f5d94dd0000000001003861',
        'https://www.xiaohongshu.com/user/profile/5af98ba74eacab4a601e97bf',
        'https://www.xiaohongshu.com/user/profile/567bb5cf6a6a6946a7ee30fb',
        'https://www.xiaohongshu.com/user/profile/5915319d82ec3972c9eabbee',
        'https://www.xiaohongshu.com/user/profile/5b0184a711be1055f3c22f27',
        'https://www.xiaohongshu.com/user/profile/5a16311de8ac2b349577ec8e',
        'https://www.xiaohongshu.com/user/profile/5a0184984eacab2b30e4dc48',
        'https://www.xiaohongshu.com/user/profile/56ba927750c4b478072c1e80',
        'https://www.xiaohongshu.com/user/profile/59f824e211be1011aa2e3c2f',
        'https://www.xiaohongshu.com/user/profile/58d0f56c6a6a696d5b2bde03',
        'https://www.xiaohongshu.com/user/profile/5a69eca311be10246f02b8a2',
        'https://www.xiaohongshu.com/user/profile/5aae4070e8ac2b068d00451d',
        'https://www.xiaohongshu.com/user/profile/5732bdf582ec390aff193608',
        'https://www.xiaohongshu.com/user/profile/59fc16cfe8ac2b264a44329a',
        'https://www.xiaohongshu.com/user/profile/5cc65e5d000000001603b3b0',
        'https://www.xiaohongshu.com/user/profile/5ae2cfe54eacab66ef5ae712',
        'https://www.xiaohongshu.com/user/profile/5baf8538631ff60001927607',
        'https://www.xiaohongshu.com/user/profile/54e5c510e779895d97956e48',
        'https://www.xiaohongshu.com/user/profile/5a65d744e8ac2b7c1aaf026c',
        'https://www.xiaohongshu.com/user/profile/5cc10d72000000001200f6b8',
        'https://www.xiaohongshu.com/user/profile/53dba221b4c4d659d329e9c1',
        'https://www.xiaohongshu.com/user/profile/5f0c0fbf0000000001001183',
        'https://www.xiaohongshu.com/user/profile/57b4421450c4b416b711f415',
        'https://www.xiaohongshu.com/user/profile/5a548fcfe8ac2b38e2616925',
        'https://www.xiaohongshu.com/user/profile/5ac33e41e8ac2b4b04fa9106',
        'https://www.xiaohongshu.com/user/profile/5888b6fb5e87e771119b1830',
        'https://www.xiaohongshu.com/user/profile/58e78fa182ec39414dc9d344',
        'https://www.xiaohongshu.com/user/profile/5b42ea314eacab4f1810db35',
        'https://www.xiaohongshu.com/user/profile/5a276b2711be1011c655976b',
        'https://www.xiaohongshu.com/user/profile/5b324e034eacab397242d0c8',
        'https://www.xiaohongshu.com/user/profile/593684c26a6a69574c1567b6',
        'https://www.xiaohongshu.com/user/profile/5950b1336a6a697709c839f7',
        'https://www.xiaohongshu.com/user/profile/54a5eff32e1d9357cd3e00e4',
        'https://www.xiaohongshu.com/user/profile/5cfa1e9a000000000602bbca',
        'https://www.xiaohongshu.com/user/profile/5eaab0f10000000001001a23',
        'https://www.xiaohongshu.com/user/profile/5c8b33b0000000001101b2f8',
        'https://www.xiaohongshu.com/user/profile/5dc438f80000000001006568',
        'https://www.xiaohongshu.com/user/profile/5a02897fe8ac2b6e94f74da9',
        'https://www.xiaohongshu.com/user/profile/5a329e5ce8ac2b0ba1fd2b29',
        'https://www.xiaohongshu.com/user/profile/58fd6ca982ec397b71885d31',
        'https://www.xiaohongshu.com/user/profile/5935627182ec394b31d24b69',
        'https://www.xiaohongshu.com/user/profile/60f59b14000000002002eb65',
        'https://www.xiaohongshu.com/user/profile/595989005e87e7786f165159',
        'https://www.xiaohongshu.com/user/profile/5b5ae24ce8ac2b1d23f69f53',
        'https://www.xiaohongshu.com/user/profile/561e0c65a75c9555e1d8d793',
    ]
    home.main(url_list)
